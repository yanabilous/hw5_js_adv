'use strict'



export class Card {
    constructor(obj, user) {

        const {body, title, id} = obj;
        const {name, email} = user;

        this.id = id;
        this.body = body;
        this.title = title;
        this.authorFullName = name;
        this.authorEmail = email;

    }

    request(method, urlSuffix = '') {
        let url = 'https://ajax.test-danit.com/api/json/posts' + urlSuffix;
        let options = {
            method: `${method}`,
            headers: {
                'Content-type': 'application/json'
            },
            body: ''
        }

        return fetch(url, options)
    }


    async delete(objId, post) {

        let result = await this.request('DELETE', `/${objId}`)
        if (result.ok) {
            post.remove()
        } else {
            throw new Error('Something goes wrong...' + result.body)
        }

    }

    render(node) {
        const htmlEl = document.createElement('div')
        htmlEl.classList.add('post')
        htmlEl.innerHTML = `
                            <span>${this.authorFullName}</span>
                            <a href="$mailto:{this.authorEmail}">${this.authorEmail}</a>
                            <h3>${this.title}</h3>
                            <p>${this.body}</p>
                            `


        let del = document.createElement('button')

        del.type = 'button'
        del.innerText = 'Delete twit'
        del.classList.add('btn')

        del.addEventListener('click', ev => { this.delete(this.id, ev.target.parentElement) })

        htmlEl.append( del)

        node.append(htmlEl);

    }

}

